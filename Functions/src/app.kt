val messageOne = "Gib mir zwei Zahlen und ich addiere sie."
val messageTwo = "Zahl 1: "
val messageThree = "Zahl 2: "
val messageFour = "Ergebnis: "

fun main(){
    println(messageOne)
    print(messageTwo)
    val number1: Int = readLine()!!.toInt()
    print(messageThree)
    val number2: Int = readLine()!!.toInt()

    val sum = add(number1, number2)

    print(messageFour)
    println(sum.toString())
}

fun add(summand1: Int, summand2: Int): Int{
    val sum = summand1 + summand2
    return sum
}