//Wir schreiben Variablen immer mit "var" am Anfang
//Kotlin weiß, dass es sich bei "number" nur um ein Int halten kann, deswegen müssen wir den Datentyp nicht mit angeben
var number = 23

//Die drei nächsten Zeilen sind also gleichbedeutend
var number1 = 69        //Int der Werte von -2147483648 bis +2147483647 annehmen kann
var number2: Int = 69   //Int der Werte von -2147483648 bis +2147483647 annehmen kann
var number3: Int? = 69  //Int der Werte von -2147483648 bis +2147483647 und null annehmen kann

//Die nächsten beiden Zeilen sind NICHT gleichbedeutend
var number4: Int? = null
var number5: Int? = 0

//Auch hier weiß Kotlin, dass es ein Char sein soll
var character = 'J'

//Schreiben wir "val" statt "var", so weiß Kotlin, dass wir diese Variable nicht weiter verändern wollen
val zeichenkette = "Hello World"

fun main(){
    //Wir können auch Variablen innerhalb einer Funktion definieren. Dann ist sie aber nur in dieser Funktion sichtbar.
    val anotherNumber = 47
    println(zeichenkette)
}

//Variablen sind immer nur für die Funktionen sichtbar, die unter der Variable stehen
//Die Main-Funktion kann diese Variable zum Beispiel nicht sehen
var floatingPoint = 4.3

//Möchten wir eine Kommazahl als Float speichern, müssen wir das Kotlin explizit mitteilen
var realFloatingPoint: Float = 4.3F